-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Máy chủ: localhost
-- Thời gian đã tạo: Th1 04, 2019 lúc 11:43 PM
-- Phiên bản máy phục vụ: 5.7.24-0ubuntu0.18.04.1
-- Phiên bản PHP: 7.1.25-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `traodoisach`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `sach`
--

CREATE TABLE `sach` (
  `IDsach` int(11) NOT NULL,
  `ISBN` int(11) NOT NULL,
  `TenSach` text NOT NULL,
  `NhaXB` varchar(255) NOT NULL,
  `NamSanXuat` int(9) NOT NULL,
  `TheLoai` text NOT NULL,
  `TacGia` text NOT NULL,
  `Gia` int(11) NOT NULL,
  `MoTa` text NOT NULL,
  `TinhTrangSach` text NOT NULL,
  `TrangThaiSach` int(11) NOT NULL,
  `DiaDiemMuaBan` text NOT NULL,
  `SoLuong` int(11) NOT NULL,
  `Active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `sach`
--

INSERT INTO `sach` (`IDsach`, `ISBN`, `TenSach`, `NhaXB`, `NamSanXuat`, `TheLoai`, `TacGia`, `Gia`, `MoTa`, `TinhTrangSach`, `TrangThaiSach`, `DiaDiemMuaBan`, `SoLuong`, `Active`) VALUES
(1, 42322435, 'Ha Hoang', 'awfasdf', 1977, 'Lich Su', 'asdfasdf ASDFAS F', 1555, 'ASDFASDFASDFAS', 'ASDFASDFDASFASDF', 0, 'QWERQWFASDFCVASDF', 0, 1),
(2, 142341234, 'dsrfefcsadf', 'asdfasdf', 12341234, 'asdfasdf', 'asdfasdf', 1341234, 'asdfasdf', 'asdfasdf', 1, 'asdfasdf', 11, 0);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `sach`
--
ALTER TABLE `sach`
  ADD PRIMARY KEY (`IDsach`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
