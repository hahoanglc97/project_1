/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project1.qltds.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.JOptionPane;


/**
 *
 * @author Admin
 */
public class ChiTietHoaDonBanController {
    public static PreparedStatement pst;
    public static ResultSet rs;
    public static Connection conn = DBConnection.getInstance().getConnection();

    public static ResultSet showTextfield(String sql) {
        try {
            pst = conn.prepareStatement(sql);
            return pst.executeQuery();
        } catch (Exception e) {
            return null;
        }
    }
    
    public static void insertChiTietHoaDonBan(String MaHoaDon, String IDsach, int ThanhTien, int SoLuong) {

        String sql = "INSERT INTO chitiethoadonban(MaHoaDon, IDsach, ThanhTien, SoLuong) "
                + "VALUES(?,?,?,?)";
        try {
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setString(1, MaHoaDon);
            pst.setString(2, IDsach);
            pst.setInt(3, ThanhTien);
            pst.setInt(4, SoLuong);
            pst.execute();
            JOptionPane.showMessageDialog(null, "Thêm chi tiết hóa đơn bán có mã sách: "+MaHoaDon+" thành công!", "Thông báo", 1);

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Mã chi tiết hóa đơn bán "+MaHoaDon+" đã tồn tại! Vui lòng thêm chi tiết hóa đơn bán khác!", "Thông báo", 2);
        }

    }
    
     public static void updateChiTietHoaDonBan(String MaHoaDon1, String MaHoaDon, String IDsach, int ThanhTien, int SoLuong) {
        String sql = " UPDATE chitiethoadonban SET MaHoaDon=N'"+MaHoaDon+"',IDsach=N'" + IDsach + "', ThanhTien=N'" + ThanhTien + "',SoLuong=N'" + SoLuong + "'";        
        try {
            pst = conn.prepareStatement(sql);
            pst.execute();
            JOptionPane.showMessageDialog(null, "Sửa chi tiết hóa đơn bán có mã "+MaHoaDon1+" thành công!");

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Sửa chi tiết hóa đơn bán không thành công!");
        }   
    }

    public static void deleteChiTietHoaDonBan(String MaHoaDon) {
        String sql = "DELETE FROM chitiethoadonban WHERE MaHoaDon='" +MaHoaDon+ "'";
        try {
            pst = conn.prepareStatement(sql);
            pst.execute();
            JOptionPane.showMessageDialog(null, "chi tiết hóa đơn bán có mã "+MaHoaDon+" đã được xóa", "Thông báo", 1);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "chi tiết hóa đơn bán có mã "+MaHoaDon+" chưa được xóa", "Thông báo", 2);
        }
    }
    
}
