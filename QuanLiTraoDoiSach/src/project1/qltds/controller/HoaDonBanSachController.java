/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project1.qltds.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.JOptionPane;


/**
 *
 * @author Admin
 */
public class HoaDonBanSachController {
    public static PreparedStatement pst;
    public static ResultSet rs;
    public static Connection conn = DBConnection.getInstance().getConnection();

    public static ResultSet showTextfield(String sql) {
        try {
            pst = conn.prepareStatement(sql);
            return pst.executeQuery();
        } catch (Exception e) {
            return null;
        }
    }
    
     public static void insertHoaDonBanSach(String MaHoaDon, String IDnguoiBan, String IDnguoiMua, String HinhThucThanhToan, String DiaDiemGiaoDich, int TongTien) {

        String sql = "INSERT INTO hoadonbansach(MaHoaDon, IDnguoiBan, IDnguoiMua, HinhThucThanhToan, DiaDiemGiaoDich, TongTien) "
                + "VALUES(?,?,?,?,?,?)";
        try {
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setString(1, MaHoaDon);
            pst.setString(2, IDnguoiBan);
            pst.setString(3, IDnguoiMua);
            pst.setString(4, HinhThucThanhToan);
            pst.setString(5, DiaDiemGiaoDich);
            pst.setInt(6, TongTien);
            pst.execute();
            JOptionPane.showMessageDialog(null, "Thêm hóa đơn bán sách có mã hóa đơn: "+MaHoaDon+" thành công!", "Thông báo", 1);

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Mã hóa đơn bán "+MaHoaDon+" đã tồn tại! Vui lòng thêm hóa đơn bán khác!", "Thông báo", 2);
        }
    }
     
     public static void updateChiTietHoaDonBan(String MaHoaDon1, String MaHoaDon, String IDsach, int ThanhTien, int SoLuong) {
        String sql = " UPDATE chitiethoadonban SET MaHoaDon=N'"+MaHoaDon+"',IDsach=N'" + IDsach + "', ThanhTien=N'" + ThanhTien + "',SoLuong=N'" + SoLuong + "'";        
        try {
            pst = conn.prepareStatement(sql);
            pst.execute();
            JOptionPane.showMessageDialog(null, "Sửa chi tiết hóa đơn bán có mã "+MaHoaDon1+" thành công!");

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Sửa chi tiết hóa đơn bán không thành công!");
        }   
    }

    public static void deleteChiTietHoaDonBan(String MaHoaDon) {
        String sql = "DELETE FROM chitiethoadonban WHERE MaHoaDon='" +MaHoaDon+ "'";
        try {
            pst = conn.prepareStatement(sql);
            pst.execute();
            JOptionPane.showMessageDialog(null, "chi tiết hóa đơn bán có mã "+MaHoaDon+" đã được xóa", "Thông báo", 1);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "chi tiết hóa đơn bán có mã "+MaHoaDon+" chưa được xóa", "Thông báo", 2);
        }
    }
}
