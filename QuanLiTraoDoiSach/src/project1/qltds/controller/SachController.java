/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project1.qltds.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import project1.qltds.model.Sach;

/**
 *
 * @author HOANG BOE
 */
public class SachController {
    public static PreparedStatement pst;
    public static ResultSet rs;
    public static Connection conn = DBConnection.getInstance().getConnection();

    public static ResultSet showTextfield(String sql) {
        try {
            pst = conn.prepareStatement(sql);
            return pst.executeQuery();
        } catch (Exception e) {
            return null;
        }
    }
    
        public static ArrayList<Sach> getDataSach() {

        ArrayList<Sach> dataSach = new ArrayList<>();
        String sql = "SELECT * FROM sach";
        ResultSet rs = null;
        try {
       java.sql.Connection conn = DBConnection.getInstance().getConnection();
            Statement statement = (Statement) conn.createStatement();
            rs = statement.executeQuery(sql);
            while (rs.next()) {
                Sach data = new Sach();
                data.setIDsach(Integer.parseInt(rs.getString("IDsach")));
                data.setISBN(rs.getString("ISBN"));
                data.setTenSach(rs.getString("TenSach"));
                data.setTacGia(rs.getString("TacGia"));
                data.setTheLoai(rs.getString("TheLoai"));
                data.setNhaXB(rs.getString("NhaXB"));
                data.setNamSanXuat(Integer.parseInt(rs.getString("NamSanXuat")));
                data.setGia(Integer.parseInt(rs.getString("Gia")));
                data.setMoTa(rs.getString("MoTa"));
                data.setTinhTrangSach(rs.getString("TinhTrangSach"));
                data.setTrangThaiSach(Integer.parseInt(rs.getString("TrangThaiSach")));
                data.setDiaDiemMuaBan(rs.getString("DiaDiemMuaBan"));
                data.setSoLuong(Integer.parseInt(rs.getString("SoLuong")));
                data.setActive(Integer.parseInt(rs.getString("Active")));
                dataSach.add(data);
            }
            rs.close();
        } catch (SQLException e) {
        }
        return (dataSach);

    }

    public static void insertSach(int IDsach, String ISBN, String TenSach, int NamSanXuat, String TheLoai, String TacGia, int Gia, String MoTa, String TinhTrangSach, String TrangThaiSach, String DieDiemMuaBan, int SoLuong, int Active) {

        String sql = "INSERT INTO Sach(IDsach, ISBN, TenSach, NamSanXuat, TheLoai, TacGia, Gia, MoTa, TinhTRangSach, TrangThaiSach, DieDiemMuaBan, SoLuong, Active) "
                + "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setInt(1, IDsach);
            pst.setString(2, ISBN);
            pst.setString(3, TenSach);
            pst.setInt(4, NamSanXuat);
            pst.setString(5, TheLoai);
            pst.setString(6, TacGia);
            pst.setInt(7, Gia);
            pst.setString(8, MoTa);
            pst.setString(9, TinhTrangSach);
            pst.setString(10, TrangThaiSach);
            pst.setString(11, DieDiemMuaBan);
            pst.setInt(12, SoLuong);
            pst.setInt(13,Active);
            pst.execute();
            JOptionPane.showMessageDialog(null, "Thêm sách có mã sách: "+IDsach+" thành công!", "Thông báo", 1);

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Mã sách "+IDsach+" đã tồn tại! Vui lòng thêm sách khác!", "Thông báo", 2);
        }

    }
    
      public static void updateSach(String IDsach1, int IDsach, String ISBN, String TenSach, int NamXuatBan, String TheLoai, String TacGia, int Gia, String MoTa, String TinhTrangSach, String TrangThaiSach, String DiaDiemMuaBan, int SoLuong, int Active) {
 String sql = " UPDATE BangSach SET IDsach=N'"+IDsach+"',ISBN=N'" + ISBN + "',TenSach=N'" + TenSach + "', , NamXuatBan=N'" +NamXuatBan+ "',TheLoai=N'" + TheLoai + "',TacGia=N'" + TacGia + "',Gia=N'" + Gia + "',MoTa=N'" + MoTa + "',TinhTrangSach=N'" + TinhTrangSach + "',TrangThaiSach=N'" + TrangThaiSach + "',DiaDiemMuaBan=N'" + DiaDiemMuaBan + "',SoLuong=N'" + SoLuong + "',Active=N'" + Active + "' WHERE IDsach='"+IDsach1+"' ";        try {
            pst = conn.prepareStatement(sql);
            pst.execute();
            JOptionPane.showMessageDialog(null, "Sửa sách có mã "+IDsach1+" thành công!");

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Sửa sách không thành công!");
        }   
    }

    public static void deleteSach(int IDsach) {
        String sql = "DELETE FROM BangSach WHERE MaSach='" +IDsach+ "'";
        try {
            pst = conn.prepareStatement(sql);
            pst.execute();
            JOptionPane.showMessageDialog(null, "Sách có mã "+IDsach+" đã được xóa", "Thông báo", 1);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Sách có mã "+IDsach+" chưa được xóa", "Thông báo", 2);
        }
    }


}
