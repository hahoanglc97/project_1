package project1.qltds.controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
    private Connection connection = null;
//    private final String CONNECTION_URL = "jdbc:mysql://localhost:3306/";
//    private final String DATABASE = "traodoisach";
//    private final String USERNAME = "root";
//    private final String PASSWORD = "28071997";
//    private final String UTF8_URL = "?useUnicode=true&characterEncoding=utf-8";
    private static final String URL = "jdbc:mysql://localhost:3306/traodoisach";
    private static final String USER = "root";
    private static final String PASSWORD = "28071997";
    private static DBConnection instance;

    public static DBConnection getInstance() {
        if(instance == null) {
            instance = new DBConnection();
        }
        return instance;
    }

    public Connection getConnection() {
        try {
            if (connection == null || connection.isClosed()) {
            connection = (Connection) DriverManager.getConnection(URL, USER, PASSWORD);
            }
        }
        catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return connection;
    }

    public void closeConnection() {
        try {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        }
        catch (SQLException sqlEx) {
            System.err.println(sqlEx.getMessage());
        }
    }
    
    public static void main(String [ ] args){
       Connection conn = DBConnection.getInstance().getConnection();
        if (conn != null){
            System.out.println("Connected");
        }
     }
}
