/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project1.qltds.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.JOptionPane;
import project1.qltds.model.ThongTinNguoiDung;

/**
 *
 * @author Admin
 */
public class ThongTinNguoiDungController {
     public static PreparedStatement pst;
    public static ResultSet rs;
    public static Connection conn = DBConnection.getInstance().getConnection();

    public static ResultSet showTextfield(String sql) {
        try {
            pst = conn.prepareStatement(sql);
            return pst.executeQuery();
        } catch (Exception e) {
            return null;
        }
    }
    
     public static void insertThongTinNguoiDung(ThongTinNguoiDung nd){

        String sql = "INSERT INTO thongtinnguoibanmuonmua(Ten,"
                + " DiaChi,"
                + " Gmail,"
                + " SoDienThoai,"
                + " AnhDaiDien,"
                + " DanhGiaUyTin,"
                + " Account,"
                + " Password,"
                + " ThongTinLichSuGiaoDich,"
                + " TrangThai) "
                + "VALUES(?,?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement pst = conn.prepareStatement(sql);
//            pst.setString(1, ID);
            pst.setString(1, nd.getTen());
            pst.setString(2, nd.getDiaChi());
            pst.setString(3, nd.getGmail());
            pst.setInt(4, nd.getSoDienThoai());
            pst.setString(5, "");
            pst.setString(6, "");
            pst.setString(7, nd.getAccount());
            pst.setString(8, nd.getPassword());
            pst.setString(9, "");
            pst.setInt(10, 0);
            pst.execute();
            JOptionPane.showMessageDialog(null, "Thêm thông tin người dùng tên: "+nd.getTen()+" thành công!", "Thông báo", 1);

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,e+ "  Mã người dùng tên "+nd.getTen()+" đã tồn tại! Vui lòng thêm thông tin người dùng khác!", "Thông báo", 2);
        }

    }

      public static void updateThongTinNguoiDung(String ID1, String ID, String Ten, String DiaChi, String Gmail, int SoDienThoai, String AnhDaiDien, String DanhGiaUyTin, String Account, String Password, String ThongTinLichSuGiaoDich, String TrangThai) {
        String sql = " UPDATE thongtinnguoibanmuonmua SET ID=N'"+ID+"',Ten=N'" + Ten + "', DiaChi=N'" + DiaChi + "',Gmail=N'" + Gmail + "', SoDienThoai=N'" + SoDienThoai + "', AnhDaiDien=N'" + AnhDaiDien + "', DanhGiaUyTin=N'" + DanhGiaUyTin + "',Account=N'" + Account + "', Password=N'" + Password + "', ThongTinLichSuGiaoDich=N'" + ThongTinLichSuGiaoDich + "', TrangThai=N'" + TrangThai + "'";        
        try {
            pst = conn.prepareStatement(sql);
            pst.execute();
            JOptionPane.showMessageDialog(null, "Sửa thông tin người dùng có mã "+ID1+" thành công!");

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Sửa thông tin người dùng không thành công!");
        }   
    }

    public static void deleteThongTinNguoiDung(String ID) {
        String sql = "DELETE FROM thongtinnguoibanmuonmua WHERE IDsach='" +ID+ "'";
        try {
            pst = conn.prepareStatement(sql);
            pst.execute();
            JOptionPane.showMessageDialog(null, " thông tin người dùng có mã "+ID+" đã được xóa", "Thông báo", 1);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, " thông tin người dùng có mã "+ID+" chưa được xóa", "Thông báo", 2);
        }
    }
}
