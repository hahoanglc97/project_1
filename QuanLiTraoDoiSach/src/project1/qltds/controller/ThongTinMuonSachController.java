/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project1.qltds.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.JOptionPane;


/**
 *
 * @author Admin
 */
public class ThongTinMuonSachController {
    public static PreparedStatement pst;
    public static ResultSet rs;
    public static Connection conn = DBConnection.getInstance().getConnection();

    public static ResultSet showTextfield(String sql) {
        try {
            pst = conn.prepareStatement(sql);
            return pst.executeQuery();
        } catch (Exception e) {
            return null;
        }
    }
    
     public static void insertThongTinMuonSach(String IDsach, String IDnguoiBan, String IDnguoiMua, int SoLuong, int SoTienDatCoc, String ThoiGianMuon, String ThoiGianTra, String TrangThai, int SoTienBoiThuong, String DiaDiemMuon){

        String sql = "INSERT INTO thongtinmuonsach(IDsach, IDnguoiBan, IDnguoiMua, SoLuong, SoTienDatCoc, ThoiGianMuon, ThoiGianTra, TrangThai, SoTienBoiThuong, DiaDiemMuon) "
                + "VALUES(?,?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setString(1, IDsach);
            pst.setString(2, IDnguoiBan);
            pst.setString(3, IDnguoiMua);
            pst.setInt(4, SoLuong);
            pst.setInt(5, SoTienDatCoc);
            pst.setString(6, ThoiGianMuon);
            pst.setString(7, ThoiGianTra);
            pst.setString(8, TrangThai);
            pst.setInt(9, SoTienBoiThuong);
            pst.setString(10, DiaDiemMuon);
            pst.execute();
            JOptionPane.showMessageDialog(null, "Thêm thông tin mượn sách có mã sách: "+IDsach+" thành công!", "Thông báo", 1);

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Mã sách "+IDsach+" đã tồn tại! Vui lòng thêm thông tin mượn sách khác!", "Thông báo", 2);
        }

    }

      public static void updateThongTinMuonSach(String IDsach1, String IDsach, String IDnguoiBan, String IDnguoiMua, int SoLuong, int SoTienDatCoc, String ThoiGianMuon, String ThoiGianTra, String TrangThai, int SoTienBoiThuong, String DiaDiemMuon) {
        String sql = " UPDATE thongtinmuonsach SET IDsach=N'"+IDsach+"',IDnguoiBan=N'" + IDnguoiBan + "', IDnguoiMua=N'" + IDnguoiMua + "',SoLuong=N'" + SoLuong + "', SoTienDatCoc=N'" + SoTienDatCoc + "', ThoiGianMuon=N'" + ThoiGianMuon + "', ThoiGianTra=N'" + ThoiGianTra + "',TrangThai=N'" + TrangThai + "', SoTienBoiThuong=N'" + SoTienBoiThuong + "', DiaDiemMuon=N'" + DiaDiemMuon + "'";        
        try {
            pst = conn.prepareStatement(sql);
            pst.execute();
            JOptionPane.showMessageDialog(null, "Sửa thông tin mượn sách có mã "+IDsach1+" thành công!");

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Sửa thông tin mượn sách không thành công!");
        }   
    }

    public static void deleteThongTinMuonSach(String IDsach) {
        String sql = "DELETE FROM thongtinmuonsach WHERE IDsach='" +IDsach+ "'";
        try {
            pst = conn.prepareStatement(sql);
            pst.execute();
            JOptionPane.showMessageDialog(null, " thông tin mượn sách có mã "+IDsach+" đã được xóa", "Thông báo", 1);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, " thông tin mượn sách có mã "+IDsach+" chưa được xóa", "Thông báo", 2);
        }
    }
}
