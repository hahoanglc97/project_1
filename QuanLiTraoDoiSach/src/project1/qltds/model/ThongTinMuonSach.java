/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project1.qltds.model;

/**
 *
 * @author HOANG BOE
 */
public class ThongTinMuonSach {
    private String IDsach;
    private String IDnguoiBan;
    private String IDnguoiMua;
    private int SoLuong;
    private int SoTienDatCoc;
    private String ThoiGianMuon;
    private String ThoiGianTra;
    private String TrangThai;
    private int SoTienBoiThuong;
    private String DiaDiemMuon;
    
    public ThongTinMuonSach(){
        
    }

    public ThongTinMuonSach(String IDsach, String IDnguoiBan, String IDnguoiMua, int SoLuong, int SoTienDatCoc, String ThoiGianMuon, String ThoiGianTra, String TrangThai, int SoTienBoiThuong, String DiaDiemMuon) {
        this.IDsach = IDsach;
        this.IDnguoiBan = IDnguoiBan;
        this.IDnguoiMua = IDnguoiMua;
        this.SoLuong = SoLuong;
        this.SoTienDatCoc = SoTienDatCoc;
        this.ThoiGianMuon = ThoiGianMuon;
        this.ThoiGianTra = ThoiGianTra;
        this.TrangThai = TrangThai;
        this.SoTienBoiThuong = SoTienBoiThuong;
        this.DiaDiemMuon = DiaDiemMuon;
    }

    public String getIDsach() {
        return IDsach;
    }

    public void setIDsach(String IDsach) {
        this.IDsach = IDsach;
    }

    public String getIDnguoiBan() {
        return IDnguoiBan;
    }

    public void setIDnguoiBan(String IDnguoiBan) {
        this.IDnguoiBan = IDnguoiBan;
    }

    public String getIDnguoiMua() {
        return IDnguoiMua;
    }

    public void setIDnguoiMua(String IDnguoiMua) {
        this.IDnguoiMua = IDnguoiMua;
    }

    public int getSoLuong() {
        return SoLuong;
    }

    public void setSoLuong(int SoLuong) {
        this.SoLuong = SoLuong;
    }

    public int getSoTienDatCoc() {
        return SoTienDatCoc;
    }

    public void setSoTienDatCoc(int SoTienDatCoc) {
        this.SoTienDatCoc = SoTienDatCoc;
    }

    public String getThoiGianMuon() {
        return ThoiGianMuon;
    }

    public void setThoiGianMuon(String ThoiGianMuon) {
        this.ThoiGianMuon = ThoiGianMuon;
    }

    public String getThoiGianTra() {
        return ThoiGianTra;
    }

    public void setThoiGianTra(String ThoiGianTra) {
        this.ThoiGianTra = ThoiGianTra;
    }

    public String getTrangThai() {
        return TrangThai;
    }

    public void setTrangThai(String TrangThai) {
        this.TrangThai = TrangThai;
    }

    public int getSoTienBoiThuong() {
        return SoTienBoiThuong;
    }

    public void setSoTienBoiThuong(int SoTienBoiThuong) {
        this.SoTienBoiThuong = SoTienBoiThuong;
    }

    public String getDiaDiemMuon() {
        return DiaDiemMuon;
    }

    public void setDiaDiemMuon(String DiaDiemMuon) {
        this.DiaDiemMuon = DiaDiemMuon;
    }
    
}
