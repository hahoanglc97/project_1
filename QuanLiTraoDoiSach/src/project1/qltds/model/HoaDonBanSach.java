/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project1.qltds.model;

/**
 *
 * @author HOANG BOE
 */
public class HoaDonBanSach {
    private String MaHoaDon;
    private String IDnguoiBan;
    private String IDnguoiMua;
    private String HinhThucThanhToan;
    private String DiaDiemGiaoDich;
    private int TongTien;
    
    public HoaDonBanSach(){
        
    }

    public HoaDonBanSach(String MaHoaDon, String IDnguoiBan, String IDnguoiMua, String HinhThucThanhToan, String DiaDiemGiaoDich, int TongTien) {
        this.MaHoaDon = MaHoaDon;
        this.IDnguoiBan = IDnguoiBan;
        this.IDnguoiMua = IDnguoiMua;
        this.HinhThucThanhToan = HinhThucThanhToan;
        this.DiaDiemGiaoDich = DiaDiemGiaoDich;
        this.TongTien = TongTien;
    }

    public String getMaHoaDon() {
        return MaHoaDon;
    }

    public void setMaHoaDon(String MaHoaDon) {
        this.MaHoaDon = MaHoaDon;
    }

    public String getIDnguoiBan() {
        return IDnguoiBan;
    }

    public void setIDnguoiBan(String IDnguoiBan) {
        this.IDnguoiBan = IDnguoiBan;
    }

    public String getIDnguoiMua() {
        return IDnguoiMua;
    }

    public void setIDnguoiMua(String IDnguoiMua) {
        this.IDnguoiMua = IDnguoiMua;
    }

    public String getHinhThucThanhToan() {
        return HinhThucThanhToan;
    }

    public void setHinhThucThanhToan(String HinhThucThanhToan) {
        this.HinhThucThanhToan = HinhThucThanhToan;
    }

    public String getDiaDiemGiaoDich() {
        return DiaDiemGiaoDich;
    }

    public void setDiaDiemGiaoDich(String DiaDiemGiaoDich) {
        this.DiaDiemGiaoDich = DiaDiemGiaoDich;
    }

    public int getTongTien() {
        return TongTien;
    }

    public void setTongTien(int TongTien) {
        this.TongTien = TongTien;
    }
    
}
