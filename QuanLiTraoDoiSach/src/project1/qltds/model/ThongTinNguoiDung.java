/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project1.qltds.model;

/**
 *
 * @author HOANG BOE
 */
public class ThongTinNguoiDung {
    private String ID;
    private String Ten;
    private String DiaChi;
    private String Gmail;
    private int SoDienThoai;
    private String AnhDaiDien;
    private String DanhGiaUyTin;
    private String Account;
    private String Password;
    private String ThongTinLichSuGiaoDich;
    private String TrangThai;  
    
    public ThongTinNguoiDung(){
        
    }

    public ThongTinNguoiDung(String ID, String Ten, String DiaChi, String Gmail, int SoDienThoai, String AnhDaiDien, String DanhGiaUyTin, String Account, String Password, String ThongTinLichSuGiaoDich, String TrangThai) {
        this.ID = ID;
        this.Ten = Ten;
        this.DiaChi = DiaChi;
        this.Gmail = Gmail;
        this.SoDienThoai = SoDienThoai;
        this.AnhDaiDien = AnhDaiDien;
        this.DanhGiaUyTin = DanhGiaUyTin;
        this.Account = Account;
        this.Password = Password;
        this.ThongTinLichSuGiaoDich = ThongTinLichSuGiaoDich;
        this.TrangThai = TrangThai;
    }
    

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getTen() {
        return Ten;
    }

    public void setTen(String Ten) {
        this.Ten = Ten;
    }

    public String getDiaChi() {
        return DiaChi;
    }

    public void setDiaChi(String DiaChi) {
        this.DiaChi = DiaChi;
    }

    public String getGmail() {
        return Gmail;
    }

    public void setGmail(String Gmail) {
        this.Gmail = Gmail;
    }

    public int getSoDienThoai() {
        return SoDienThoai;
    }

    public void setSoDienThoai(int SoDienThoai) {
        this.SoDienThoai = SoDienThoai;
    }

    public String getAnhDaiDien() {
        return AnhDaiDien;
    }

    public void setAnhDaiDien(String AnhDaiDien) {
        this.AnhDaiDien = AnhDaiDien;
    }

    public String getDanhGiaUyTin() {
        return DanhGiaUyTin;
    }

    public void setDanhGiaUyTin(String DanhGiaUyTin) {
        this.DanhGiaUyTin = DanhGiaUyTin;
    }

    public String getAccount() {
        return Account;
    }

    public void setAccount(String Account) {
        this.Account = Account;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    public String getThongTinLichSuGiaoDich() {
        return ThongTinLichSuGiaoDich;
    }

    public void setThongTinLichSuGiaoDich(String ThongTinLichSuGiaoDich) {
        this.ThongTinLichSuGiaoDich = ThongTinLichSuGiaoDich;
    }

    public String getTrangThai() {
        return TrangThai;
    }

    public void setTrangThai(String TrangThai) {
        this.TrangThai = TrangThai;
    }
    
}

    
