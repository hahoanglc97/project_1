/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project1.qltds.model;

/**
 *
 * @author HOANG BOE
 */
public class ChiTietHoaDonBan {
    private String MaHoaDon;
    private String IDsach;
    private int ThanhTien;
    private int SoLuong;
    
    public ChiTietHoaDonBan(){
        
    }

    public ChiTietHoaDonBan(String MaHoaDon, String IDsach, int ThanhTien, int SoLuong) {
        this.MaHoaDon = MaHoaDon;
        this.IDsach = IDsach;
        this.ThanhTien = ThanhTien;
        this.SoLuong = SoLuong;
    }

    public String getMaHoaDon() {
        return MaHoaDon;
    }

    public void setMaHoaDon(String MaHoaDon) {
        this.MaHoaDon = MaHoaDon;
    }

    public String getIDsach() {
        return IDsach;
    }

    public void setIDsach(String IDsach) {
        this.IDsach = IDsach;
    }

    public int getThanhTien() {
        return ThanhTien;
    }

    public void setThanhTien(int ThanhTien) {
        this.ThanhTien = ThanhTien;
    }

    public int getSoLuong() {
        return SoLuong;
    }

    public void setSoLuong(int SoLuong) {
        this.SoLuong = SoLuong;
    }
    
}
