/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project1.qltds.model;

import java.io.Serializable;
import project1.qltds.controller.Giaotiep;

/**
 *
 * @author HOANG BOE
 */
public class Sach implements Serializable, Giaotiep {
    private int IDsach;
    private String ISBN;
    private String TenSach;
    private String NhaXB;
    private int NamSanXuat;
    private String TheLoai;
    private String TacGia;
    private int Gia;
    private String MoTa;
    private String TinhTrangSach;
    private int TrangThaiSach;
    private String DiaDiemMuaBan;
    private int SoLuong;
    private int Active;
    
    public Sach(){
        
    }

    public Sach(int IDsach, String ISBN, String TenSach, String NhaXB, int NamSanXuat, String TheLoai, String TacGia, int Gia, String MoTa, String TinhTrangSach, int TrangThaiSach, String DiaDiemMuaBan, int SoLuong, int Active) {
        this.IDsach = IDsach;
        this.ISBN = ISBN;
        this.TenSach = TenSach;
        this.NhaXB = NhaXB;
        this.NamSanXuat = NamSanXuat;
        this.TheLoai = TheLoai;
        this.TacGia = TacGia;
        this.Gia = Gia;
        this.MoTa = MoTa;
        this.TinhTrangSach = TinhTrangSach;
        this.TrangThaiSach = TrangThaiSach;
        this.DiaDiemMuaBan = DiaDiemMuaBan;
        this.SoLuong = SoLuong;
        this.Active = Active;
    }
      

    public int getIDsach() {
        return IDsach;
    }

    public void setIDsach(int IDsach) {
        this.IDsach = IDsach;
    }

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public String getTenSach() {
        return TenSach;
    }

    public void setTenSach(String TenSach) {
        this.TenSach = TenSach;
    }
    
    public String getNhaXB(){
        return NhaXB;
    }
    
    public void setNhaXB(String NhaXB){
        this.NhaXB = NhaXB;
    }

    public int getNamSanXuat() {
        return NamSanXuat;
    }

    public void setNamSanXuat(int NamSanXuat) {
        this.NamSanXuat = NamSanXuat;
    }

    public String getTheLoai() {
        return TheLoai;
    }

    public void setTheLoai(String TheLoai) {
        this.TheLoai = TheLoai;
    }

    public String getTacGia() {
        return TacGia;
    }

    public void setTacGia(String TacGia) {
        this.TacGia = TacGia;
    }

    public int getGia() {
        return Gia;
    }

    public void setGia(int Gia) {
        this.Gia = Gia;
    }

    public String getMoTa() {
        return MoTa;
    }

    public void setMoTa(String MoTa) {
        this.MoTa = MoTa;
    }

    public String getTinhTrangSach() {
        return TinhTrangSach;
    }

    public void setTinhTrangSach(String TinhTrangSach) {
        this.TinhTrangSach = TinhTrangSach;
    }

    public int getTrangThaiSach() {
        return TrangThaiSach;
    }

    public void setTrangThaiSach(int TrangThaiSach) {
        this.TrangThaiSach = TrangThaiSach;
    }

    public String getDiaDiemMuaBan() {
        return DiaDiemMuaBan;
    }

    public void setDiaDiemMuaBan(String DiaDiemMuaBan) {
        this.DiaDiemMuaBan = DiaDiemMuaBan;
    }

    public int getSoLuong() {
        return SoLuong;
    }

    public void setSoLuong(int SoLuong) {
        this.SoLuong = SoLuong;
    }  
    
    public int getActive(){
        return Active;
    }
    
    public void setActive(int Active){
        this.Active = Active;
    }

    @Override
    public Object toObject() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
